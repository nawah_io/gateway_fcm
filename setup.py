from gateway_fcm import __api_level__, __version__

import setuptools


with open('README.md', 'r') as f:
	long_description = f.read()

with open('./requirements.txt', 'r') as f:
	requirements = f.readlines()

setuptools.setup(
	name='gateway_fcm',
	version=__version__,
	author='Mahmoud Abduljawad',
	author_email='mahmoud@masaar.com',
	description='Nawah Gateway for Firebase Cloud Messaging (FCM)',
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/nawah_io/gateway_fcm',
	package_data={
		'gateway_fcm': ['requirements.txt'],
	},
	packages=[
		'gateway_fcm',
	],
	project_urls={
		'Docs: Gitlab': 'https://gitlab.com/nawah_io/gateway_fcm',
		'Gitlab: issues': 'https://gitlab.com/nawah_io/gateway_fcm/-/issues',
		'Gitlab: repo': 'https://gitlab.com/nawah_io/gateway_fcm',
	},
	classifiers=[
		'Programming Language :: Python :: 3',
		'Programming Language :: Python :: 3.8',
		'Development Status :: 5 - Production/Stable',
		'License :: OSI Approved :: GNU Affero General Public License v3 or later (GPLv3+)',
		'Operating System :: OS Independent',
		'Topic :: Internet :: WWW/HTTP',
		'Framework :: AsyncIO',
	],
	python_requires='>=3.8',
	install_requires=requirements,
)
